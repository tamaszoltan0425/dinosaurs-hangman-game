package org.fasttrackit;

import java.util.Arrays;
import java.util.Scanner;


public class Main {

    public static final String LETTER = "Enter a letter: ";
    public static final String LIFE = "You lose a life!";
    public static final String EXTINCT = "Same as the dinosaurs, you are now EXTINCT!";
    public static final String CONGRATS = "Congrats! You must have enjoyed Jurassic Park as a kid.";
    public static final String WELCOME = "Guess the dinosaur!";

    public static final String[] dinosaurs = {"velociraptor", "diplodocus", "pterodactyl", "stegosaurus", "brontosaurus", "tyrannosaurus", "triceratops"};

    public static void main(String[] args) {

        int min = 0;
        int max = args.length - 1;
        int argRandom = (int) (Math.random() * (max - min + 1) + min); //generates a random number between 0 and 6, so the word to be guessed is given from program arguments array

        System.out.println(WELCOME);
        System.out.println();
        String word = dinosaurs[argRandom];
        String wordToPlay = word.toUpperCase();

        char[] charArray = wordToPlay.toCharArray(); // build an array of character out of string
        char[] invisibleChars = new char[charArray.length];


        for (int i = 0; i < invisibleChars.length; i++) {
            invisibleChars[i] = '*';
            System.out.print(invisibleChars[i] + " ");
        }

        System.out.println();


        int livesCounter = 3;
        String livesLeft = "***";


        while (livesCounter > 0) {          //I had the Arrays.equals(invisibleChars, charArray) as OR condition in the loop, did not work, kept running until livesCounter = 0.

            Scanner a = new Scanner(System.in);
            System.out.println();
            System.out.println(LETTER);
            String letterGiven = a.next();
            char letterToCh = letterGiven.charAt(0);
            char letterToPlay = Character.toUpperCase(letterToCh);

            int count = 0; // every time the program switches '*' with letterToPlay, we increment the counter

            for (int i = 0; i < invisibleChars.length; i++) {
                if (invisibleChars[i] == '*') {
                    if (letterToPlay == charArray[i]) {
                        invisibleChars[i] = letterToPlay;
                        count++;

                    }
                }
            }

            for (int i = 0; i < invisibleChars.length; i++) {
                System.out.print(invisibleChars[i] + " ");

            }


            if (count == 0) {
                System.out.println();
                System.out.println(LIFE);
                livesCounter -= 1;
                livesLeft = livesLeft.substring(0, livesLeft.length() - 1);
                System.out.println();
                System.out.println("Lives Left: " + livesLeft);
            } else {
                System.out.println();
                System.out.println("Lives left: " + livesLeft);
            }
            if (livesCounter == 0) {
                System.out.println(EXTINCT);
                System.out.println();
                System.out.println("The answer was: " + wordToPlay);


            }
            if (Arrays.equals(invisibleChars, charArray)) {
                System.out.println();
                System.out.println(CONGRATS);
                break;
            }

        }


    }
}
// other fun ideas to implement:
// 3 levels:
// beginner: with 3 random letters in charArray provided already
// intermediate: the first letter of charArray provided
//Jurassic Guru: as is, invisibleChars only

// build an array (or string?) of wrong letters already guessed